FROM node:14.14

WORKDIR /srv/

ADD ./ ./

RUN yarn install

RUN yarn run

EXPOSE 80

ENTRYPOINT NODE_ENV=production node ./dist/build.js
