import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import validator from 'validator';
import { GraphQLClient } from 'graphql-request';
import passwordValidator from 'password-validator';
import { User } from './models';
require('dotenv').config();

var passwordSchema = new passwordValidator();

passwordSchema
    .is()
    .min(8)
    .is()
    .max(200)
    .has()
    .not()
    .spaces()
    .is()
    .not()
    .oneOf(['Passw0rd', 'Password123']);

const graphQLClient = new GraphQLClient(process.env.HASURA_ENDPOINT, {
    headers: {
        'X-Hasura-Admin-Secret': process.env.HASURA_GRAPHQL_ADMIN_SECRET,
    },
});

/*
    Some constants
*/

/*
    Creating the Express instance
*/

const app = express();
const port = process.env.NODE_ENV === 'production' ? 80 : 3001;

/*
    Adding middleware to Express
    This is necessary to get the post
    data from the request and to 
    access the API from a different 
    host 
*/

app.use(bodyParser.json());
app.use(cors());

/*
    Express routes/endpoints
*/

app.post('/get-token', async (req, res) => {
    const { email, password } = req.body;
    if (!email || !password) {
        res.status(400).send({
            success: false,
            message: 'Email or password missing in the request',
        });
    } else {
        try {
            const data: { users: User[] } = await graphQLClient.request(
                findUserQuery,
                {
                    email: email,
                }
            );

            if (data.users.length === 0) {
                res.status(404).send({
                    success: false,
                    message: `Could not find account: ${email}`,
                });
            } else {
                const user = data.users[0];
                const match = await bcrypt.compare(
                    password,
                    user.password_hash
                );
                if (match) {
                    //we create the JWT for the user with our secret
                    //inside the token we encrypt some user data
                    //then we send the token to the user
                    const token = jwt.sign(
                        {
                            id: user.id,
                            'https://hasura.io/jwt/claims': {
                                'x-hasura-allowed-roles': [user.role.slug],
                                'x-hasura-default-role': user.role.slug,
                                'x-hasura-user-id': user.id.toString(),
                                // "x-hasura-org-id": "123",
                                // "x-hasura-custom": "custom-value"
                            },
                        },
                        process.env.HASH_KEY,
                        {
                            expiresIn: '7d',
                        }
                    );

                    res.send({
                        success: true,
                        token: token,
                        expiresIn: Date.now() + 1000 * 60 * 60 * 24 * 7,
                        user: {
                            id: user.id,
                            email: user.email.toLowerCase(),
                            allowed_roles: user.role.slug,
                            default_role: user.role.slug,
                        },
                    });
                } else {
                    //return error to user to let them know the password is incorrect
                    res.status(401).send({
                        success: false,
                        message: 'Incorrect credentials',
                    });
                }
            }
        } catch (e) {
            console.log(e);
            res.status(500).send({
                success: false,
                message: 'Unexpected error',
            });
        }
    }
});

app.post('/create-account', async (req, res) => {
    const { email, password } = req.body;

    if (!email || !password) {
        res.status(400).send({
            success: false,
            message: 'Name, email or password missing in the request',
        });
    }

    try {
        const data: { users: User[] } = await graphQLClient.request(
            findUserQuery,
            {
                email: email,
            }
        );
        if (data.users.length > 0) {
            res.status(400).send({
                success: false,
                message: `Account with email ${email} already exits`,
            });
        } else if (!validator.isEmail(email)) {
            res.status(400).send({
                success: false,
                message: `You provided a unvalid emailaddress`,
            });
        } else if (!passwordSchema.validate(password)) {
            res.status(400).send({
                success: false,
                message: `You provided a unvalid password, the password has to have a minimun of 8  and a maximun of 200 characters. Spaces are not allowed`,
            });
        } else {
            const hash = await bcrypt.hash(
                password,
                Number(process.env.SALT_ROUNDS)
            );
            const newUser: {
                insert_users_one: User;
            } = await graphQLClient.request(createUserMutation, {
                input: {
                    email: email.toLowerCase(),
                    password_hash: hash,
                    role_id: 2,
                },
            });

            const user = newUser.insert_users_one;

            const token = jwt.sign(
                {
                    id: user.id,
                    'https://hasura.io/jwt/claims': {
                        'x-hasura-allowed-roles': [user.role.slug],
                        'x-hasura-default-role': user.role.slug,
                        'x-hasura-user-id': user.id.toString(),
                    },
                },
                process.env.HASH_KEY,
                {
                    expiresIn: '7d',
                }
            );
            res.send({
                success: true,
                token: token,
                expiresIn: Date.now() + 1000 * 60 * 60 * 24 * 7,
                user: {
                    id: user.id,
                    email: user.email.toLowerCase(),
                    allowed_roles: user.role.slug,
                    default_role: user.role.slug,
                },
            });
        }
    } catch (e) {
        console.error(e);
        res.status(500).send({
            success: false,
            message: 'Something went wrong',
            error: e,
        });
    }
});

/*
    Starting the app
*/
app.listen(port, () =>
    console.log(`DNDCS: Auth service is running on port ${port}!`)
);

const findUserQuery = `
    query CheckUser($email: String) {
    users(where: {email: {_eq: $email}}){
        id
        email
        password_hash
        role{
            id
            slug
        }
    }
  }
  `;

const createUserMutation = `
  mutation CreateUser($input: users_insert_input! ) {
    insert_users_one(object: $input){
        
            id
            email
            role{
                id
                slug
            }
           
          }
    
  }
`;
