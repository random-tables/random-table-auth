export type User = {
    id: number;
    email: string;
    password_hash: string;
    created_at: string;
    updated_at: string;
    role: {
        id: number;
        slug: string;
    }
};
