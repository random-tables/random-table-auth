const path = require('path');
var nodeExternals = require('webpack-node-externals')

module.exports = {
  node: {
    fs: 'empty',
    net: 'empty',
  },
  mode: 'development',
  target: 'node',
  externals: [nodeExternals()],
  entry: './src/app.ts',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    filename: 'build.js',
    path: path.resolve(__dirname, 'dist'),
  },
};